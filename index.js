 var http = require('http'); 

  http.createServer(function (req, res) { 
    res.writeHead(200, {'Content-Type': 'text/html'}); 
    res.end(`
  <html>
    <head><title>My simple node app</title></head>
    <body>
      <h2>Hello Ghost Inspector!</h2>
      <p>This is just a simple Node.js app.</p>
    </body>
  </html>  
    `); 
  }).listen(8000);
  console.log('Server running on port 8000.');